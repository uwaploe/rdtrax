package main

// Keep a running average of a map of floating-point values
type accumulator struct {
	avg, avgsq map[string]float32
	count      int
}

func newAccumulator() *accumulator {
	return &accumulator{
		avg:   make(map[string]float32),
		avgsq: make(map[string]float32),
	}
}

// Add a new sample to the running average.
func (a *accumulator) addSample(rec map[string]float32) int {
	a.count++
	alpha := 1. / float32(a.count)
	beta := 1.0 - alpha
	for k, v := range rec {
		a.avg[k] = v*alpha + beta*a.avg[k]
		a.avgsq[k] = v*v*alpha + beta*a.avg[k]
	}
	return a.count
}

// Reset the accumulator
func (a *accumulator) reset() {
	a.count = 0
	a.avg = make(map[string]float32)
	a.avgsq = make(map[string]float32)
}

// Return a map containing the current average values
func (a *accumulator) getMean() map[string]float32 {
	return a.avg
}
