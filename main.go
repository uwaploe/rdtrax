// Rdtrax samples the PNI TRAX2 attitude sensor and publishes the data
// records to a Redis pub-sub channel.
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"
	"text/template"
	"time"

	tcm "bitbucket.org/mfkenney/go-tcm"
	"github.com/gomodule/redigo/redis"
	"github.com/pkg/errors"
	"github.com/tarm/serial"
)

var Version = "dev"
var BuildDate = "unknown"

var Usage = `Usage: rdtrax [options] serialdev interval

Read data records from a TRAX2 attitude sensor attached to SERIALDEV and
publish the data values to a Redis pub-sub channel in CSV format.
`

// Default data record components
const FIELDS = "heading,pangle,rangle"

// Default template for the output CSV records
const FORMAT = `{{.T|tfmt}},{{.D.pangle|printf "%.1f"}},{{.D.rangle|printf "%.1f"}},{{.D.heading|printf "%.1f"}}`

type dataFields []tcm.ComponentId

func (f *dataFields) String() string {
	return fmt.Sprint(*f)
}

func (f *dataFields) Set(value string) error {
	names := make([]string, 0)
	for _, name := range strings.Split(value, ",") {
		if !tcm.IsValidComponent(name) {
			return fmt.Errorf("Invalid field name: %q", name)
		}
		names = append(names, name)
	}

	*f = tcm.ComponentIds(names...)
	return nil
}

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	debug     bool
	dataFmt   string
	recFields dataFields
	mountRef  string = "std0"
	rdAddr    string = "localhost:6379"
	rdChan    string = "data.tcm"
	inBaud    int    = 38400
	lowPower  bool
)

func initializeDevice(dev *tcm.Trax, fields dataFields, ref string) error {
	err := dev.SetDataComponents(fields...)
	if err != nil {
		return errors.Wrap(err, "SetDataComponents")
	}
	mref, err := tcm.MountingRefValue(ref)
	if err != nil {
		return errors.Wrap(err, "mounting ref")
	}
	err = dev.SetConfig(tcm.Kmountingref, mref)
	if err != nil {
		return errors.Wrap(err, "SetConfig")
	}
	err = dev.SetAcqParams(tcm.Polled, true, 0, 0)
	if err != nil {
		return errors.Wrap(err, "SetAcqParams")
	}
	dev.SetMagRef()

	err = dev.Save()
	if err != nil {
		return errors.Wrap(err, "Save")
	}
	return nil
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	recFields = tcm.ComponentIds("heading", "pangle", "rangle")
	flag.BoolVar(&debug, "debug", debug, "Output diagnostic information while running")
	flag.BoolVar(&lowPower, "sleep", lowPower, "Sleep between samples")
	flag.StringVar(&dataFmt, "fmt", dataFmt, "Output data format template")
	flag.Var(&recFields, "fields", "Output data record fields")
	flag.StringVar(&mountRef, "ref", mountRef, "TRAX2 mounting reference value")
	flag.StringVar(&rdAddr, "rdaddr", rdAddr, "Redis server HOST:PORT")
	flag.StringVar(&rdChan, "rdchan", rdChan, "Redis pub-sub channel")
	flag.IntVar(&inBaud, "baud", inBaud, "TRAX2 baud rate")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCmdLine()

	if len(args) < 2 {
		flag.Usage()
		os.Exit(1)
	}

	// Drop log timestamps when running under Systemd
	if _, ok := os.LookupEnv("JOURNAL_STREAM"); ok {
		log.SetFlags(0)
	}

	if dataFmt == "" {
		dataFmt = buildTemplate(recFields)
	}

	fm := template.FuncMap{
		"tfmt": formatTime,
	}
	tmpl := template.Must(template.New("rec").Funcs(fm).Parse(dataFmt))

	cfg := &serial.Config{
		Name:        args[0],
		Baud:        inBaud,
		ReadTimeout: time.Second * 5,
	}
	port, err := serial.OpenPort(cfg)
	if err != nil {
		log.Fatalf("Serial port open failed: %v", err)
	}

	dev := tcm.NewTrax(port)
	info, err := dev.GetModInfo()
	if err != nil {
		log.Fatalf("No response from TRAX2: %v", err)
	}

	err = initializeDevice(dev, recFields, mountRef)
	if err != nil {
		log.Fatalf("Cannot initialize TRAX2: %v", err)
	}

	if lowPower {
		dev.SetMode(tcm.CompassMode)
		err = dev.PowerDown()
		if err != nil {
			log.Fatalf("Cannot put TRAX2 in low-power sleep: %v", err)
		}
	}

	interval, err := time.ParseDuration(args[1])
	if err != nil {
		log.Fatalf("Invalid interval, %q: %v", args[1], err)
	}
	dev.SetMergeRate(interval)

	var conn redis.Conn
	if rdChan != "" {
		conn, err = redis.Dial("tcp", rdAddr)
		if err != nil {
			log.Fatalf("Cannot connect to Redis: %v", err)
		}
		defer conn.Close()
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	log.Printf("DOT Buoy TRAX2 data reader (%s)", Version)
	log.Printf("TRAX2 module: %s", info)

	ticker := time.NewTicker(interval)
	for {
		select {
		case t0 := <-ticker.C:
			if lowPower {
				err = dev.PowerUp()
				if err != nil {
					log.Fatalf("PowerUp failed: %v", err)
				}
			}

			vals, err := dev.GetData()
			if err != nil {
				log.Fatalf("GetData failed: %v", err)
			}

			if lowPower {
				dev.PowerDown()
			}

			if rdChan != "" {
				publishMessage(conn, rdChan, tmpl, newDataRecord(t0, vals))
			} else {
				fmt.Printf("%s\n", createMessage(tmpl, newDataRecord(t0, vals)))
			}
		case <-sigs:
			return
		}
	}

}
