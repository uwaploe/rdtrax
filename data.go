package main

import (
	"bytes"
	"fmt"
	"strings"
	"text/template"
	"time"

	tcm "bitbucket.org/mfkenney/go-tcm"
	"github.com/gomodule/redigo/redis"
)

type dataRecord struct {
	T time.Time
	D map[string]float32
}

func buildTemplate(fields dataFields) string {
	var b strings.Builder
	fmt.Fprint(&b, "{{.T|tfmt}}")
	for _, f := range fields {
		fmt.Fprintf(&b, ",{{.D.%s|printf \"%%.1f\"}}", strings.ToLower(f.String()))
	}
	return b.String()
}

// Template formatting function
func formatTime(t time.Time) string {
	return t.UTC().Format("2006-01-02 15:04:05.999")
}

func newDataRecord(t time.Time, vals []tcm.DataValue) dataRecord {
	dr := dataRecord{T: t, D: make(map[string]float32)}
	for _, val := range vals {
		name := strings.ToLower(val.Id.String())
		dr.D[name], _ = val.Data.(float32)
	}
	return dr
}

func createMessage(t *template.Template, dr dataRecord) []byte {
	var buf bytes.Buffer
	t.Execute(&buf, dr)
	return buf.Bytes()
}

func publishMessage(conn redis.Conn, chName string, t *template.Template,
	dr dataRecord) {
	conn.Do("PUBLISH", chName, createMessage(t, dr))
}
