PACKAGE  = rdtrax
DATE    ?= $(shell date +%FT%T%z)
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || \
			cat $(CURDIR)/.version 2> /dev/null || echo v0)
BIN      = $(CURDIR)/bin

GO      = go
export GO111MODULE=on

.PHONY: all
all: $(BIN)
	$(GO) build \
		-tags release \
		-ldflags '-X main.Version=$(VERSION) -X main.BuildDate=$(DATE)' \
		-o $(shell realpath --relative-to=. $(BIN)/$(PACKAGE)) main.go

$(BIN):
	@mkdir -p $@

.PHONY: clean
clean:
	@rm -rf $(BIN)
