module bitbucket.org/uwaploe/rdtrax

require (
	bitbucket.org/mfkenney/go-tcm v1.6.0
	github.com/gomodule/redigo v1.8.5
	github.com/pkg/errors v0.8.1
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	golang.org/x/sys v0.0.0-20190215142949-d0b11bdaac8a // indirect
)

go 1.13
